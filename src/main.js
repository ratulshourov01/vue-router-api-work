import Vue from 'vue'
import App from './App.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
    // Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.productionTip = false

import VueRouter from 'vue-router'
import { routerAll } from './route';
Vue.use(VueRouter)
const router = new VueRouter({
    routes: routerAll,
    mode: 'history'
})
new Vue({
    render: h => h(App),
    router: router
}).$mount('#app')