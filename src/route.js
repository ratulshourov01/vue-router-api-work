import Home from '@/components/Home';
import About from '@/components/About';
import Contact from '@/components/Contact';

export const routerAll = [{
        path: '/home',
        component: Home,
    },
    {
        path: '/about/:id',
        component: About,
    },
    {
        path: '/contact',
        component: Contact,
    }

];